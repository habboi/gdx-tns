package com.habboi.tns.rendering.shaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g3d.Attributes;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.PointLightsAttribute;
import com.badlogic.gdx.graphics.g3d.shaders.DefaultShader;
import com.badlogic.gdx.graphics.g3d.utils.BaseShaderProvider;

/*
 * Provides per-fragment lighting, as libgdx's DefaultShader
 * gives us only per-vertex lighting.
 */
public class LitShader extends DefaultShader {

    private Attributes combinedAttributes = new Attributes();

    public LitShader(Renderable r, Config c) {
        super(r, c);
    }

    public static class Provider extends BaseShaderProvider {
        @Override
        protected Shader createShader(Renderable renderable) {
            DefaultShader.Config config = new DefaultShader.Config(
                    Gdx.files.internal("shaders/lit.vert.glsl").readString(),
                    Gdx.files.internal("shaders/lit.frag.glsl").readString()
            );

            PointLightsAttribute attr = (PointLightsAttribute) renderable.environment.get(PointLightsAttribute.Type);

            if (attr != null)
                config.numPointLights = attr.lights.size;
            else
                config.numPointLights = 0;
                
            return new DefaultShader(renderable, config);
        }
    }
}
